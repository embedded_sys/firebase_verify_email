package com.test.androidemailverify;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;

public class MainActivity extends AppCompatActivity {

    private static final int PER_LOGIN = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder().build(), PER_LOGIN);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PER_LOGIN) {
            handleSignInResponse(resultCode, data);
            return;
        }
    }

    private void handleSignInResponse(int requestCode, Intent data) {
        if (requestCode == RESULT_OK) {
            Intent newActivity = new Intent(MainActivity.this, Status.class);
            startActivity(newActivity);
            finish();
            return;
        } else
            Toast.makeText(this, "Failed to login ...", Toast.LENGTH_SHORT).show();
    }
}
